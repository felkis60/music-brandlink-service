package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	vk_music "miltilinks-go/vk_music"
	"net/http"
	"strings"
	"time"
)

func VKMusicFindQuery(query string) vk_music.VkMusicResponse {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "http://api.moosic.io/search/?q="+query+"&limit=10", nil)
	req.Header.Set("User-Agent", "okhttp/5.0.0-alpha.2")
	req.Header.Set("Authorization", "Bearer eefe5d09cb17d43ff34749a033721a1e5f50801af54e76d158dc1da312cc2d5a")
	res, _ := client.Do(req)
	b, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err)
	}

	response := vk_music.VkMusicResponse{}
	err = json.Unmarshal(b, &response)
	if err != nil {
		fmt.Println(err)
	}

	return response
}

func VKMusicQueue(c chan QueryTask) {
	defer VKMusicQueue(c)
	for {
		task, ok := <-c
		if ok == false {
			log.Println("[VKMusicQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("VKMusic" + task.Query)
			if len(task.Query) > 0 {
				data := VKMusicFindQuery(task.Query)
				url := VKMusicGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func VKMusicGetUrl(data vk_music.VkMusicResponse, query string) string {
	vkMusicUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	for i, item := range data.Data.Tracks {
		if !findIt {
			artist := strings.ReplaceAll(item.ArtistDisplayName, "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[VK MUSIC] Artist %d: %s\n", i, artist)
			log.Printf("[VK MUSIC] ArtistIdx %d: %d\n", i, artistIdx)

			track := strings.ReplaceAll(item.Name, "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[VK MUSIC] Track %d: %s\n", i, track)
			log.Printf("[VK MUSIC] TrackIdx %d: %d\n", i, trackIdx)

			if artistIdx > -1 && trackIdx > -1 {
				url := "https://share.boom.ru/track/" + item.APIID + "/?share_auth=" + item.ShareHash
				log.Printf("[VK MUSIC] url %d: %s\n", i, url)
				vkMusicUrl = url

				findIt = true
			}
		}
	}

	return vkMusicUrl
}
