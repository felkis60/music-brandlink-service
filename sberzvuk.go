package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func SberzvukFindQuery(ctx context.Context, query string) {
	url := "https://zvuk.com/search?query=" + query

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func SberzvukQueue(c chan QueryTask) {
	defer SberzvukQueue(c)
	ctx := SberzvukPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[SberzvukQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Sber" + task.Query)
			if len(task.Query) > 0 {
				SberzvukFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "sberzvuk")
				url := SberzvukGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func SberzvukPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://zvuk.com"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func SberzvukGetUrl(data string, query string) string {
	SberzvukUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".sc-1jlvdlc-10").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".bqBOiz")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[SBERZVUK] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[SBERZVUK] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".jbattU")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[SBERZVUK] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[SBERZVUK] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := trackEl.Find("a").Attr("href")
				if exist {
					log.Printf("[SBERZVUK] url %d: %s\n", i, url)
					SberzvukUrl = "https://zvuk.com" + url
					findIt = true
				}
			}
		}
	})

	return SberzvukUrl
}
