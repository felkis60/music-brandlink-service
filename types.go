package main

type BrandLink struct {
	AppleMusicUrl   string `json:"apple_music_url"`
	VkUrl           string `json:"vk_url"`
	YandexUrl       string `json:"yandex_url"`
	ShazamUrl       string `json:"shazam_url"`
	YoutubeUrl      string `json:"youtube_url"`
	YoutubeMusicUrl string `json:"youtube_music_url"`
	VkMusicUrl      string `json:"vk_music_url"`
	SpotifyUrl      string `json:"spotify_url"`
	DeezerUrl       string `json:"deezer_url"`
	AmazonUrl       string `json:"amazon_url"`
	SberzvukUrl     string `json:"sberzvuk_url"`
}
