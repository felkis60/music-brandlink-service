package deezer

type DeezerResponse struct {
	Data  []Datum `json:"data"`
	Total int64   `json:"total"`
}

type Datum struct {
	ID                    int64       `json:"id"`
	Readable              bool        `json:"readable"`
	Title                 string      `json:"title"`
	TitleShort            string      `json:"title_short"`
	TitleVersion          string      `json:"title_version"`
	Link                  string      `json:"link"`
	Duration              int64       `json:"duration"`
	Rank                  int64       `json:"rank"`
	ExplicitLyrics        bool        `json:"explicit_lyrics"`
	ExplicitContentLyrics int64       `json:"explicit_content_lyrics"`
	ExplicitContentCover  int64       `json:"explicit_content_cover"`
	Preview               string      `json:"preview"`
	Md5Image              string      `json:"md5_image"`
	Artist                ArtistClass `json:"artist"`
	Album                 AlbumClass  `json:"album"`
	Type                  DatumType   `json:"type"`
}

type AlbumClass struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Cover       string    `json:"cover"`
	CoverSmall  string    `json:"cover_small"`
	CoverMedium string    `json:"cover_medium"`
	CoverBig    string    `json:"cover_big"`
	CoverXl     string    `json:"cover_xl"`
	Md5Image    string    `json:"md5_image"`
	Tracklist   string    `json:"tracklist"`
	Type        AlbumType `json:"type"`
}

type ArtistClass struct {
	ID            int64      `json:"id"`
	Name          string     `json:"name"`
	Link          string     `json:"link"`
	Picture       string     `json:"picture"`
	PictureSmall  string     `json:"picture_small"`
	PictureMedium string     `json:"picture_medium"`
	PictureBig    string     `json:"picture_big"`
	PictureXl     string     `json:"picture_xl"`
	Tracklist     string     `json:"tracklist"`
	Type          ArtistType `json:"type"`
}

type AlbumType string

const (
	Album AlbumType = "album"
)

type ArtistType string

const (
	Artist ArtistType = "artist"
)

type DatumType string

const (
	Track DatumType = "track"
)
