package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/chromedp/chromedp"
	"github.com/gin-gonic/gin"
)

var reg string
var logging string

const dir = "/var/www/music-brandlink-service/"
const maxWait = 15

type AppleMusicQueueItem struct {
	C     chan string
	Query string
}

type QueryTask struct {
	C     chan string
	Query string
}

type Queue struct {
	AppleMusicQueue   chan QueryTask
	VKQueue           chan QueryTask
	YandexQueue       chan QueryTask
	ShazamQueue       chan QueryTask
	YoutubeMusicQueue chan QueryTask
	VKMusicQueue      chan QueryTask
	SpotifyQueue      chan QueryTask
	DeezerQueue       chan QueryTask
	AmazonQueue       chan QueryTask
	SberzvukQueue     chan QueryTask
}

// Mutex
func main() {
	// flags declaration using flag package
	flag.StringVar(&reg, "r", "ru", "")
	flag.StringVar(&logging, "l", "n", "")
	flag.Parse()

	f, err := os.OpenFile(dir+"log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	log.SetOutput(f)

	queue := Queue{
		AppleMusicQueue:   make(chan QueryTask),
		VKQueue:           make(chan QueryTask),
		YandexQueue:       make(chan QueryTask),
		ShazamQueue:       make(chan QueryTask),
		YoutubeMusicQueue: make(chan QueryTask),
		VKMusicQueue:      make(chan QueryTask),
		SpotifyQueue:      make(chan QueryTask),
		DeezerQueue:       make(chan QueryTask),
		AmazonQueue:       make(chan QueryTask),
		SberzvukQueue:     make(chan QueryTask),
	}
	go StartQueue(queue)

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		appleMusicTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		vkTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		yandexTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		shazamTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		youtubeMusicTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		vkMusicTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		spotifyTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		deezerTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		amazonTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}
		sberzvukTask := QueryTask{
			C:     make(chan string),
			Query: c.Query("query"),
		}

		var b BrandLink
		var wg sync.WaitGroup

		if reg == "ru" {
			wg.Add(6)
			go func() {
				defer wg.Done()
				queue.AppleMusicQueue <- appleMusicTask
				b.AppleMusicUrl = <-appleMusicTask.C
			}()
			go func() {
				defer wg.Done()
				queue.VKQueue <- vkTask
				b.VkUrl = <-vkTask.C
			}()
			go func() {
				defer wg.Done()
				queue.YandexQueue <- yandexTask
				b.YandexUrl = <-yandexTask.C
			}()
			go func() {
				defer wg.Done()
				queue.VKMusicQueue <- vkMusicTask
				b.VkMusicUrl = <-vkMusicTask.C
			}()
			go func() {
				defer wg.Done()
				queue.YoutubeMusicQueue <- youtubeMusicTask
				b.YoutubeMusicUrl = <-youtubeMusicTask.C
			}()
			go func() {
				defer wg.Done()
				queue.SberzvukQueue <- sberzvukTask
				b.SberzvukUrl = <-sberzvukTask.C
			}()
		}
		if reg == "en" {
			wg.Add(4)
			go func() {
				defer wg.Done()
				queue.ShazamQueue <- shazamTask
				b.ShazamUrl = <-shazamTask.C
			}()
			go func() {
				defer wg.Done()
				queue.SpotifyQueue <- spotifyTask
				b.SpotifyUrl = <-spotifyTask.C
			}()
			go func() {
				defer wg.Done()
				queue.DeezerQueue <- deezerTask
				b.DeezerUrl = <-deezerTask.C
			}()
			go func() {
				defer wg.Done()
				queue.AmazonQueue <- amazonTask
				b.AmazonUrl = <-amazonTask.C
			}()
		}

		if waitTimeout(&wg, time.Second*12) {
			log.Println("Timed out waiting for wait group")
			c.JSON(http.StatusOK, gin.H{
				"brandlink": b,
			})
		} else {
			log.Println("Wait group finished")
			c.JSON(http.StatusOK, gin.H{
				"brandlink": b,
			})
		}
	})
	r.Run(":4010")
}

func StartQueue(queue Queue) {
	if reg == "ru" {
		go AppleMusicQueue(queue.AppleMusicQueue)
		go VKQueue(queue.VKQueue)
		go YandexQueue(queue.YandexQueue)
		go YoutubeMusicQueue(queue.YoutubeMusicQueue)
		go VKMusicQueue(queue.VKMusicQueue)
		go SberzvukQueue(queue.SberzvukQueue)
	}
	if reg == "en" {
		go ShazamQueue(queue.ShazamQueue)
		go SpotifyQueue(queue.SpotifyQueue)
		go DeezerQueue(queue.DeezerQueue)
		go AmazonQueue(queue.AmazonQueue)
	}
}

func TakeDataScreenshotAndLogFromCtx(ctx context.Context, query string, prefix string) string {
	var data string

	if logging == "y" {
		var screenshot []byte

		if err := chromedp.Run(ctx,
			chromedp.Sleep(time.Second*3),
			chromedp.CaptureScreenshot(&screenshot),
			chromedp.OuterHTML("html", &data, chromedp.ByQuery),
		); err != nil {
			log.Fatal(err)
		}

		ioutil.WriteFile(dir+"tmp/"+prefix+"_log__"+query+".txt", []byte(data), 0644)
		ioutil.WriteFile(dir+"tmp/"+prefix+"_screenshot__"+query+".jpg", []byte(screenshot), 0644)
	} else {
		if err := chromedp.Run(ctx,
			chromedp.Sleep(time.Second*3),
			chromedp.OuterHTML("html", &data, chromedp.ByQuery),
		); err != nil {
			log.Fatal(err)
		}
	}

	return data
}

func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}
}
