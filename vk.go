package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func VKFindQuery(ctx context.Context, query string) {
	url := "https://vk.com/search?c%5Bper_page%5D=200&c%5Bq%5D=" + query + "&c%5Bqid%5D=287003566313101531&c%5Bsection%5D=audio"

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func VKQueue(c chan QueryTask) {
	defer VKQueue(c)
	ctx := VKPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[VKQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("VK" + task.Query)
			if len(task.Query) > 0 {
				VKFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "vk")
				url := VKGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func VKPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://vk.com/login"),
		chromedp.SendKeys("input[name=login]", "79146784619"),
		chromedp.Click("button[type=submit]"),
		chromedp.Sleep(time.Second*3),
		chromedp.SendKeys("input[name=password]", "Mf1x2pw446v7"),
		chromedp.Click("button[type=submit]"),
		chromedp.Sleep(time.Second*3),
	); err != nil {

		log.Fatal(err)
	}

	return ctx
}

func VKGetUrl(data string, query string) string {
	VKUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".audio_row").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			dataAudio, exist := s.Attr("data-audio")
			if exist {
				log.Printf("[VK] DATA AUDIO %d: %s\n", i, dataAudio)
				artistIdx := strings.Index(strings.ReplaceAll(dataAudio, "ё", "е"), strings.Split(artistQuery, " ")[0])
				log.Printf("[VK] Artist Index %d: %d\n", i, artistIdx)
				trackIdx := strings.Index(strings.ReplaceAll(dataAudio, "ё", "е"), strings.Split(trackQuery, " ")[0])
				log.Printf("[VK] Track Index %d: %d\n", i, trackIdx)

				if artistIdx > -1 && trackIdx > -1 {
					for dataAudioIdx, dataAudioValue := range strings.Split(dataAudio, "\"\",") {
						if !findIt {
							log.Printf("[VK] DATA_AUDIO %d: %s\n", dataAudioIdx, dataAudioValue)
							dataAudioValue = strings.ReplaceAll(dataAudioValue, "[", "")
							dataAudioValue = strings.ReplaceAll(dataAudioValue, "]", "")
							dataAudioValueIds := strings.Split(dataAudioValue, ",")
							if (dataAudioIdx == 0 || dataAudioIdx == 4) && len(dataAudioValueIds) > 3 {
								VKUrl = "https://vk.com/music/album/" + dataAudioValueIds[0] + "_" + dataAudioValueIds[1] + "_" + strings.ReplaceAll(dataAudioValueIds[2], "\"", "")
								log.Printf("[VK] url %d: %s\n", i, VKUrl)
								findIt = true
							} else {
								dataFullId, exist := s.Attr("data-full-id")
								if exist && dataFullId[0] == '-' {
									VKUrl = "https://vk.com/audio" + dataFullId
								}
							}
							// if dataAudioIdx == 6 && len(dataAudioValueIds) > 3 {
							// 	dataFullId, exist := s.Attr("data-full-id")
							// 	if exist {
							// 		VKUrl = "https://vk.com/audio" + dataFullId + "_" + strings.ReplaceAll(dataAudioValueIds[5], "\"", "")
							// 		log.Printf("[VK] url %d: %s\n", i, VKUrl)
							// 		findIt = true
							// 	}
							// }
						}
					}
				}
			}
		}

	})

	return VKUrl
}
