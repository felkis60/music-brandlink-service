package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/device"
)

func YoutubeMusicFindQuery(ctx context.Context, query string) {
	url := "https://music.youtube.com/search?q=" + query

	if err := chromedp.Run(ctx,
		chromedp.Emulate(device.IPhoneX),
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func YoutubeMusicQueue(c chan QueryTask) {
	defer YoutubeMusicQueue(c)
	ctx := YoutubeMusicPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[YoutubeMusicQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("YoutubeMusic" + task.Query)
			if len(task.Query) > 0 {
				YoutubeMusicFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "youtube_music")
				url := YoutubeMusicGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func YoutubeMusicPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://music.youtube.com"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func YoutubeMusicGetUrl(data string, query string) string {
	youtubeMusicUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".ytmusic-shelf-renderer").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".secondary-flex-columns")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[YOUTUBE MUSIC] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[YOUTUBE MUSIC] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".title")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[YOUTUBE MUSIC] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[YOUTUBE MUSIC] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := s.Find(".yt-simple-endpoint").Attr("href")
				if exist {
					log.Printf("[YOUTUBE MUSIC] url %d: %s\n", i, url)
					youtubeMusicUrl = "https://music.youtube.com/" + url

					findIt = true
				}
			}
		}
	})

	return youtubeMusicUrl
}
