package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func SpotifyFindQuery(ctx context.Context, query string) {
	url := "https://open.spotify.com/search/" + query

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.Sleep(time.Second*3),
		chromedp.Click("[data-testid=tracklist-row] [data-testid=more-button]"),
	); err != nil {
		log.Fatal(err)
	}
}

func SpotifyQueue(c chan QueryTask) {
	defer SpotifyQueue(c)
	ctx := SpotifyPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[SpotifyQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Spotify" + task.Query)
			if len(task.Query) > 0 {
				SpotifyFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "Spotify")
				url := SpotifyGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func SpotifyPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://open.spotify.com"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func SpotifyGetUrl(data string, query string) string {
	spotifyUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find("[data-testid=tracklist-row]").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			if i == 0 {
				artistEl := s
				artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
				artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
				log.Printf("[SPOTIFY] Artist Index %d: %d\n", i, artistIdx)
				log.Printf("[SPOTIFY] Artist %d: %s\n", i, artist)
				trackEl := s
				track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
				trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
				log.Printf("[SPOTIFY] Track Index %d: %d\n", i, trackIdx)
				log.Printf("[SPOTIFY] Track %d: %s\n", i, track)

				if artistIdx > -1 && trackIdx > -1 {
					doc.Find(".DuEPSADpSwCcO880xjUG").Each(func(j int, submenu *goquery.Selection) {
						if strings.Index(submenu.Text(), "К альбому") > -1 {
							url, exist := submenu.Find("a	").Attr("href")
							if exist {
								log.Printf("[SPOTIFY] url %d: %s\n", i, url)
								spotifyUrl = "https://open.spotify.com" + url

								findIt = true
							}
						}
					})
				}
			}
		}
	})

	doc.Find(".E1N1ByPFWo4AJLHovIBQ").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".Za_uNH8nTZ0qCuIqbPLZ")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[SPOTIFY] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[SPOTIFY] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".Nqa6Cw3RkDMV8QnYreTr")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[SPOTIFY] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[SPOTIFY] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := trackEl.Attr("href")
				if exist {
					log.Printf("[SPOTIFY] url %d: %s\n", i, url)
					spotifyUrl = "https://open.spotify.com" + url

					findIt = true
				}
			}
		}
	})

	return spotifyUrl
}
