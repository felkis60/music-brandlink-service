package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"miltilinks-go/deezer"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func DeezerFindQuery(ctx context.Context, query string) {
	url := "https://api.deezer.com/search?q=" + query

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func DeezerGetResponse(data string) deezer.DeezerResponse {
	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	response := deezer.DeezerResponse{}

	err = json.Unmarshal([]byte(doc.Find("pre").Text()), &response)
	if err != nil {
		fmt.Println(err)
	}

	return response
}

func DeezerQueue(c chan QueryTask) {
	defer DeezerQueue(c)
	ctx := DeezerPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[DeezerQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Deezer" + task.Query)
			if len(task.Query) > 0 {
				DeezerFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "deezer")
				response := DeezerGetResponse(data)
				url := DeezerGetUrl(response, task.Query)
				task.C <- url
			}
		}
	}
}

func DeezerPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://api.deezer.com"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func DeezerGetUrl(data deezer.DeezerResponse, query string) string {
	deezerUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	for i, item := range data.Data {
		if !findIt {
			artist := strings.ReplaceAll(item.Artist.Name, "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[DEEZER] Artist %d: %s\n", i, artist)
			log.Printf("[DEEZER] ArtistIdx %d: %d\n", i, artistIdx)

			track := strings.ReplaceAll(item.Title, "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[DEEZER] Track %d: %s\n", i, track)
			log.Printf("[DEEZER] TrackIdx %d: %d\n", i, trackIdx)

			if artistIdx > -1 && trackIdx > -1 {
				url := item.Link
				log.Printf("[DEEZER] url %d: %s\n", i, url)
				deezerUrl = url

				findIt = true
			}
		}
	}

	return deezerUrl
}
