package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func AmazonFindQuery(ctx context.Context, query string) {
	url := "https://www.amazon.com/s?k=" + query + "&crid=UCNJ7MP1NQVE&sprefix=%2Caps%2C650&ref=nb_sb_noss_2"

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func AmazonQueue(c chan QueryTask) {
	defer AmazonQueue(c)
	ctx := AmazonPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[AmazonQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Amazon" + task.Query)
			if len(task.Query) > 0 {
				AmazonFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "Amazon")
				url := AmazonGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func AmazonPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://www.amazon.com"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func AmazonGetUrl(data string, query string) string {
	amazonUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".sg-row").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".a-row.a-size-base.a-color-secondary")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[AMAZON] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[AMAZON] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".a-link-normal.s-underline-text.s-underline-link-text.s-link-style.a-text-normal")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[AMAZON] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[AMAZON] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := trackEl.Attr("href")
				if exist {
					log.Printf("[AMAZON] url %d: %s\n", i, url)
					amazonUrl = "https://www.amazon.com" + url

					findIt = true
				}
			}
		}
	})

	return amazonUrl
}
