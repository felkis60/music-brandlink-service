package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func ShazamFindQuery(ctx context.Context, query string) {
	if err := chromedp.Run(ctx,
		chromedp.Clear("input[type=search]"),
		chromedp.SendKeys("input[type=search]", query),
	); err != nil {
		log.Fatal(err)
	}
}

func ShazamQueue(c chan QueryTask) {
	defer ShazamQueue(c)
	ctx := ShazamPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[ShazamQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Shazam" + task.Query)
			if len(task.Query) > 0 {
				ShazamFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "shazam")
				url := ShazamGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func ShazamPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://www.shazam.com/ru"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func ShazamGetUrl(data string, query string) string {
	shazamUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".search-track-menu-item").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".subtitle")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[SHAZAM] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[SHAZAM] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".title")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[SHAZAM] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[SHAZAM] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := s.Attr("href")
				if exist {
					log.Printf("[SHAZAM] url %d: %s\n", i, url)
					shazamUrl = url

					findIt = true
				}
			}
		}
	})

	return shazamUrl
}
