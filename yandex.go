package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func YandexFindQuery(ctx context.Context, query string) {
	url := "https://music.yandex.ru/search?text=" + query

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func YandexQueue(c chan QueryTask) {
	defer YandexQueue(c)
	ctx := YandexPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[YandexQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			fmt.Println("Yandex" + task.Query)
			if len(task.Query) > 0 {
				YandexFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "yandex")
				url := YandexGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func YandexPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://music.yandex.ru"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func YandexGetUrl(data string, query string) string {
	yandexUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".d-track__overflowable-wrapper").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".deco-link")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[YANDEX] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[YANDEX] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".d-track__title")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[YANDEX] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[YANDEX] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := trackEl.Attr("href")
				if exist {
					log.Printf("[YANDEX] url %d: %s\n", i, url)
					yandexUrl = "https://music.yandex.ru" + url

					findIt = true
				}
			}
		}
	})

	return yandexUrl
}
