package vk_music

type VkMusicResponse struct {
	Data Data `json:"data"`
}

type Data struct {
	Counts      DataCounts     `json:"counts"`
	BlocksOrder []string       `json:"blocksOrder"`
	Albums      []AlbumElement `json:"albums"`
	Tracks      []Track        `json:"tracks"`
	Playlists   []Playlist     `json:"playlists"`
	Artists     []interface{}  `json:"artists"`
	QueryIDS    QueryIDS       `json:"queryIds"`
}

type AlbumElement struct {
	ShareHash            string          `json:"shareHash"`
	APIID                string          `json:"apiId"`
	Duration             int64           `json:"duration"`
	Types                []string        `json:"types"`
	Counts               AlbumCounts     `json:"counts"`
	ArtistDisplayName    string          `json:"artistDisplayName"`
	UpdatedAt            int64           `json:"updatedAt"`
	ReleaseDateTimestamp int64           `json:"releaseDateTimestamp"`
	UMATags              string          `json:"umaTags"`
	Cover                Cover           `json:"cover"`
	IsExplicit           bool            `json:"isExplicit"`
	Artists              []ArtistElement `json:"artists"`
	Description          interface{}     `json:"description"`
	Year                 int64           `json:"year"`
	Name                 string          `json:"name"`
	Tags                 []interface{}   `json:"tags"`
	AddedAt              int64           `json:"addedAt"`
	Permissions          Permissions     `json:"permissions"`
	IsRadioCapable       bool            `json:"isRadioCapable"`
	IsLiked              bool            `json:"isLiked"`
}

type ArtistElement struct {
	APIID          string       `json:"apiId"`
	ShareHash      string       `json:"shareHash"`
	UMATags        interface{}  `json:"umaTags"`
	IsLiked        bool         `json:"isLiked"`
	Avatar         Cover        `json:"avatar"`
	IsAutoGenCover bool         `json:"isAutoGenCover"`
	IsRadioCapable bool         `json:"isRadioCapable"`
	UpdatedAt      int64        `json:"updatedAt"`
	AddedAt        int64        `json:"addedAt"`
	Name           string       `json:"name"`
	Counts         ArtistCounts `json:"counts"`
}

type Cover struct {
	URL         string  `json:"url"`
	AccentColor *string `json:"accentColor"`
	AvgColor    *string `json:"avgColor"`
}

type ArtistCounts struct {
	NewTrack int64 `json:"newTrack"`
}

type AlbumCounts struct {
	Like  int64 `json:"like"`
	Track int64 `json:"track"`
}

type Permissions struct {
	Permit bool   `json:"permit"`
	Reason string `json:"reason"`
}

type DataCounts struct {
	Album  int64 `json:"album"`
	Track  int64 `json:"track"`
	Artist int64 `json:"artist"`
}

type Playlist struct {
	IsEnhanced     bool           `json:"isEnhanced"`
	SpecialCover   Cover          `json:"specialCover"`
	Counts         PlaylistCounts `json:"counts"`
	UpdatedAt      int64          `json:"updatedAt"`
	Type           string         `json:"type"`
	Source         string         `json:"source"`
	APIID          string         `json:"apiId"`
	Duration       int64          `json:"duration"`
	ShareHash      string         `json:"shareHash"`
	AddedAt        int64          `json:"addedAt"`
	IsOldBoom      bool           `json:"isOldBoom"`
	IsDownloads    bool           `json:"isDownloads"`
	Name           string         `json:"name"`
	IsLiked        bool           `json:"isLiked"`
	IsFavorite     bool           `json:"isFavorite"`
	IsDefault      bool           `json:"isDefault"`
	BadSync        bool           `json:"badSync"`
	IsRadioCapable bool           `json:"isRadioCapable"`
	IsExplicit     bool           `json:"isExplicit"`
	Cover          Cover          `json:"cover"`
	Celebrity      bool           `json:"celebrity"`
}

type PlaylistCounts struct {
	Like  int64 `json:"like"`
	Track int64 `json:"track"`
	Play  int64 `json:"play"`
}

type QueryIDS struct {
	Artist   string `json:"artist"`
	Album    string `json:"album"`
	Track    string `json:"track"`
	Playlist string `json:"playlist"`
}

type Track struct {
	Album             TrackAlbum    `json:"album"`
	ArtistDisplayName string        `json:"artistDisplayName"`
	Counts            TrackCounts   `json:"counts"`
	ShareHash         string        `json:"shareHash"`
	VkAudioID         string        `json:"vk_audio_id"`
	Duration          int64         `json:"duration"`
	APIID             string        `json:"apiId"`
	File              string        `json:"file"`
	IsLegal           bool          `json:"isLegal"`
	Lyrics            Lyrics        `json:"lyrics"`
	IsRadioCapable    bool          `json:"isRadioCapable"`
	Artist            TrackArtist   `json:"artist"`
	Permissions       Permissions   `json:"permissions"`
	IsRestricted      bool          `json:"isRestricted"`
	IsLiked           bool          `json:"isLiked"`
	Name              string        `json:"name"`
	Size              int64         `json:"size"`
	Artists           []TrackArtist `json:"artists"`
	IsAdded           bool          `json:"isAdded"`
	Cover             Cover         `json:"cover"`
	IsExplicit        bool          `json:"isExplicit"`
}

type TrackAlbum struct {
	APIID string `json:"apiId"`
	Cover Cover  `json:"cover"`
	Name  string `json:"name"`
}

type TrackArtist struct {
	APIID          string `json:"apiId"`
	Avatar         Cover  `json:"avatar"`
	IsAutoGenCover bool   `json:"isAutoGenCover"`
	Name           string `json:"name"`
}

type TrackCounts struct {
	Play int64 `json:"play"`
}

type Lyrics struct {
}
