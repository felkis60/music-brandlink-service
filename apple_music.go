package main

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
)

func AppleMusicFindQuery(ctx context.Context, query string) {
	url := "https://music.apple.com/ru/search?term=" + query

	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
	); err != nil {
		log.Fatal(err)
	}
}

func AppleMusicQueue(c chan QueryTask) {
	ctx := AppleMusicPrepare()

	for {
		task, ok := <-c
		if ok == false {
			log.Println("[AppleMusicQueue] ", ok, "<-- loop broke!")
			break // exit break loop
		} else {
			go func() {
				time.Sleep(maxWait * time.Second)
				task.C <- ""
			}()
			if len(task.Query) > 0 {
				AppleMusicFindQuery(ctx, task.Query)
				data := TakeDataScreenshotAndLogFromCtx(ctx, task.Query, "apple_music")
				url := AppleMusicGetUrl(data, task.Query)
				task.C <- url
			}
		}
	}
}

func AppleMusicPrepare() context.Context {
	ctx, _ := chromedp.NewContext(context.Background())

	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://music.apple.com/ru"),
	); err != nil {
		log.Fatal(err)
	}

	return ctx
}

func AppleMusicGetUrl(data string, query string) string {
	appleMusicUrl := ""
	findIt := false
	splitQuery := strings.Split(query, "-")
	artistQuery := strings.TrimSpace(splitQuery[0])
	trackQuery := strings.TrimSpace(splitQuery[1])

	r := strings.NewReader(data)
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".product-lockup__content-details").Each(func(i int, s *goquery.Selection) {
		if !findIt {
			artistEl := s.Find(".product-lockup__subtitle")
			artist := strings.ReplaceAll(artistEl.Text(), "ё", "е")
			artistIdx := strings.Index(artist, strings.Split(artistQuery, " ")[0])
			log.Printf("[APPLE MUSIC] Artist Index %d: %d\n", i, artistIdx)
			log.Printf("[APPLE MUSIC] Artist %d: %s\n", i, artist)
			trackEl := s.Find(".product-lockup__title")
			track := strings.ReplaceAll(trackEl.Text(), "ё", "е")
			trackIdx := strings.Index(track, strings.Split(trackQuery, " ")[0])
			log.Printf("[APPLE MUSIC] Track Index %d: %d\n", i, trackIdx)
			log.Printf("[APPLE MUSIC] Track %d: %s\n", i, track)

			if artistIdx > -1 && trackIdx > -1 {
				url, exist := trackEl.Attr("href")
				if exist {
					log.Printf("[APPLE MUSIC] %s\n", url)
					appleMusicUrl = url
					findIt = true
				}
			}
		}
	})

	return appleMusicUrl
}
